# Prometheus on OpenShift

Folder contents of 'openshift-prometheus': 
 - node-exporter: Docker containers which will collect metric from the Master/Infra/Compute nodes
 - configmaps: example configuration for Prometheus
 - alertmanager: Deployment configuration of Alertmanager


----
oc new-project prometheus --display-name="Prometheus Monitoring"
----


----
oc annotate namespace prometheus openshift.io/node-selector=""
----

----
ansible-playbook -i /etc/ansible/hosts ./openshift-prometheus/setup.yml
----

----
oc set env dc router -n default --list|grep STATS_PASSWORD|awk -F"=" '{print $2}'
----

----
oc new-app -f prometheus.yaml --param ROUTER_PASSWORD=<Router Password>
----

----
oc adm policy add-scc-to-user privileged system:serviceaccount:prometheus:prometheus
----

Make sure your Prometheus pod is run on the node which has the Router installed, In our case this is the ComputeNode

----
oc get pod -o wide
----

## Next Steps

Add the following annotation to your deployment of an application which you want to monitor

annotations:
  prometheus.io/path: /prometheus
  prometheus.io/port: "8080"
  prometheus.io/scrape: "true"

- Install Grafana
- Import Grafana Dashboards
